import { Component } from "react";
import Products from "../Product/Products";

class MenuContainer extends Component {
  render() {
    const {
      currentSale,
      filteredProducts,
      productsName,
      productsPrice,
      productsCategory,
      productsImg,
      handleClick,
      productsKey,
    } = this.props;
    return (
      <Products
        productsName={productsName}
        productsPrice={productsPrice}
        productsImg={productsImg}
        productsCategory={productsCategory}
        filteredProducts={filteredProducts}
        currentSale={currentSale}
        handleClick={handleClick}
        productsKey={productsKey}
      />
    );
  }
}
export default MenuContainer;
