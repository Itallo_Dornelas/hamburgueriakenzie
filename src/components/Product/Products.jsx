import { Component } from "react";
class Products extends Component {
  render() {
    const {
      productsName,
      productsPrice,
      productsCategory,
      productsImg,
      handleClick,
      productsKey,
    } = this.props;
    return (
      <div>
        <div className="container">
          <div className="imagens">
            <img src={productsImg} alt="productsImg" />
          </div>
          <h1>{productsName}</h1>
          <h2>R${productsPrice}</h2>
          <h3>{productsCategory}</h3>

          <button onClick={() => handleClick(productsKey)}>Adicionar</button>
        </div>
      </div>
    );
  }
}
export default Products;
