import { Component } from "react";
class ContainerMenu extends Component {
  render() {
    const {
      productsName,
      productsPrice,
      productsCategory,
      productsImg,
    } = this.props;
    return (
      <div>
        <div className="containerMenu">
          <div className="imagens">
            <img src={productsImg} alt="productsImg" />
          </div>
          <h1>{productsName}</h1>
          <h2>R${productsPrice}</h2>
          <h3>{productsCategory}</h3>
        </div>
      </div>
    );
  }
}
export default ContainerMenu;
