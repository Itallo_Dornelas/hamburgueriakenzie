import { Component } from "react";
import "./App.css";
import "./components/Header/header.css";
import { GiHamburger } from "react-icons/gi";
import MenuContainer from "./components/MenuContainer/MenuContainer";
import { products } from "./components/prodsConst";
import ContainerMenu from "./components/ContainerMenu/ContainerMenu";

class App extends Component {
  state = {
    products,
    filteredProducts: [],
    valorInput: "",
    currentSale: { total: 0, saleDetails: [] },
  };
  showProducts = () => {
    const { filteredProducts, valorInput } = this.state;
    if (filteredProducts.length === 0) {
      this.setState({
        filteredProducts: [...filteredProducts, valorInput],
      });
    } else {
      this.setState({
        filteredProducts: [],
        valorInput: "",
      });
    }
  };
  handleClick = (productId) => {
    const { saleDetails, total } = this.state.currentSale;
    this.setState({
      currentSale: {
        total: total,
        saleDetails: [
          ...saleDetails,
          products.find((item) => item.id === productId),
        ],
      },
    });
  };
  showSale = () => {};

  render() {
    const {
      products,
      valorInput,
      filteredProducts,
      currentSale: { saleDetails },
    } = this.state;
    return (
      <main className="header">
        <GiHamburger className="icons" />
        <h1 className="title">Hamburgueria Kenzie</h1>
        <input
          value={valorInput}
          type="text"
          onChange={(e) => this.setState({ valorInput: e.target.value })}
        />
        <button onClick={this.showProducts}>
          {filteredProducts.length === 0 ? " Pesquisar " : " Refazer Busca "}
        </button>
        <div className="App">
          {products
            .filter((item) => {
              if (filteredProducts.length === 0) {
                return (
                  <MenuContainer
                    productsKey={item.id}
                    productsName={item.name}
                    productsPrice={item.price}
                    productsCategory={item.category}
                    productsImg={item.image}
                    handleClick={this.handleClick}
                  />
                );
              }
              return item.name === valorInput;
            })
            .map((item) => {
              return (
                <MenuContainer
                  productsKey={item.id}
                  productsName={item.name}
                  productsPrice={item.price}
                  productsCategory={item.category}
                  productsImg={item.image}
                  handleClick={this.handleClick}
                />
              );
            })}
        </div>
        <div className="wrapperMenu">
          <h1 className="titles">SubTotal-</h1>
          <h2 className="titles">
            R$
            {saleDetails.reduce((acumulator, currentValue) => {
              return (
                parseFloat(currentValue.price.toFixed(2)) +
                parseFloat(acumulator.toFixed(2))
              );
            }, 0)}
          </h2>
        </div>

        <div className="wrapperMenu">
          {saleDetails.map((item) => {
            return (
              <ContainerMenu
                productsKey={item.id}
                productsName={item.name}
                productsPrice={item.price}
                productsCategory={item.category}
                productsImg={item.image}
              />
            );
          })}
        </div>
      </main>
    );
  }
}

export default App;
